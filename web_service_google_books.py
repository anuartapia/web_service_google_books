#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Este programa utiliza la API Books para el proyecto Google Books, que es
# un proyecto cuyo objetivo es digitalizar todos los libros del mundo.

import sys
import pprint

# Biblioteca de las APIs de Google para Python
from apiclient.discovery import build

# Llave API generada por el desarrollador
api_key = "AIzaSyD0xaG7FkCp97slKSL6i3SSWvWQYgfHPXs"

# La función apiclient.discovery.build() regresa una instancia de un objeto de
# servicio API que puede ser usado para hacer llamadas API. El objeto es construido
# con los métodos específicos para la API de Google Books.
# Los argumentos que le pasamos son:
#	nombre de la API: books
#	versión de la API: v1
#	llave API
service = build('books', 'v1', developerKey=api_key)

# La API Books tiene un método volumes().list() que es usado para listar
# libros dado un criterio de búsqueda.
# Los parámetros que le pasamos son:
#	fuente de volúmenes: public
#	consulta de búsqueda: [query personalizado]
# El método regresa un objeto apiclient.http.HttpRequest que encapsula
# toda la información necesaria para hacer la petición, pero no llama a la API.
query = raw_input("Ingresa los térmios de búsqueda:\n")
request = service.volumes().list(source='public', q=query)

# La función execute() en el objeto HttpRequest es la que en realidad llama a la API-
# Regresa un objeto Python construido desde la respuesta JSON. Para conocer la
# estructura del mismo podemos consultar la documentación de API Books.
response = request.execute()
#pprint.pprint(response)

# Accesamos a la respuesta como un objeto dict con una llave 'items' que regresa una
# lista de objetos items (libros). El objeto item es un objeto dict con una llave
# 'volumeInfo'. El objeto volumneInfo es un dict con llaves 'title' y 'authors'.
print 'Encontrados %d libros:' % len(response['items'])
for book in response.get('items', []):
  print '======================================================================='
  print ' Titulo:\n"%s"\n Autores:\n%s\n Descripcion:\n%s' % (
    book['volumeInfo']['title'],
    book['volumeInfo']['authors'],
    book['volumeInfo']['description'])
